#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

///////////////////////gcc -pthread  -o mth.o multithread.c
int arr[10] = {2,4,1,6,5,3,7,9,8,0};
pthread_t tid[4];
int max=0;
int avr=0;
void *DoWorkInThr1(void *arg){
	int x;
	int i;
	max = arr[0];
	for(x=0;x<0xFFFFFFF;x++){
		for(i=1;i<10;i++){
			if(max < arr[i]){
				max = arr[i];		
			}
		}
	}
    return NULL;
}
void *DoWorkInThr2(void *arg){
	int x;
	int i;
	int sum;
	for(x=0;x<0xFFFFFFF;x++){
		sum=0;
		for(i=0;i<10;i++){
			sum=sum+arr[i];
		}
		avr=sum/10;
	}
}
int main(void)
{
    int i = 0;
    int err;


        err = pthread_create(&(tid[0]), NULL, &DoWorkInThr1, NULL);
        err = pthread_create(&(tid[1]), NULL, &DoWorkInThr2, NULL);
   //Main
     printf(" Doing in Main\n");
   // DoWorkInMainThr();
    /* block until all threads complete */
  for (i = 0; i < 2; ++i) {
    pthread_join(tid[i], NULL);
  }
 
    printf("max= %d  avr = %d\n",max,avr);
    return 0;
}
//gcc   -pthread  -o tr3.o tr3.c

